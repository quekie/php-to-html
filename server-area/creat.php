﻿<?php
/**
 * Example Application
 *
 * @package Example-application
 */

require './smarty/libs/Smarty.class.php';

$smarty = new Smarty;



        $smarty->template_dir="./smarty/templates";	//指定模版存放目录
        $smarty->compile_dir="./smarty/templates_c";	//指定编译文件存放目录
        $smarty->config_dir="./smarty/config";	//指定配置文件存放目录
        $smarty->cache_dir="./smarty/cache";	//指定缓存存放目录

$smarty->left_delimiter = "{"; 
$smarty->right_delimiter = "}"; 

 $smarty->assign("name", "changhao"); //进行模板变量替换 

//在当前页面显示
// $smarty->display("index.tpl"); //编译并显示位于./templates下的index.htm模板 

//输出到html文件
$content = $smarty->fetch("index.tpl");
//这里的 fetch() 就是获取输出内容的函数,现在$content变量里面,就是要显示的内容了
$fp = fopen("../client-area/output.html", "w+");
fwrite($fp, $content);
fclose($fp);
?>
